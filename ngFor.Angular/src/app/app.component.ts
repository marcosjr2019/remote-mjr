import { Component } from '@angular/core';
import { Lista } from './lista';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  pessoas  = [
    new Lista(1, 'José Athur', 'Garçon', true),
    new Lista(2, 'Mario Covas', 'Operário Textil', false),
    new Lista(3, 'Marcos Aguiar', 'Musico', true),
    new Lista(4, 'Ivo Mendes', 'Motorista', true)
  ] 
}
